#Purpose of This Repo
The perpose of this repo is to show my on going progress on docker to future potential employers.

#Minimal Application That Implements Microservices Architecture

The application demonstrate how it is easy to make various systems talking to each other independently (losely coupled).
Three componets are used in order to build this application:

1. python:3.4 official image
1. amouat/dnmonster:1.0 image based on nodejs to generate random identicon images
1. redis:3.0 cache key/value pair mem cache application built using C language

The images are hooked togather using docker-composer.yml which orchastrate the building, attaching volumes, port listening and linking between these 3 independent containers

Also, the Dockerfile is used to build the python image and has two mutual exclusive routines to indicate whether on production or development.

The source code of this application is from Oreilly's Media [Using Docker](http://shop.oreilly.com/product/0636920035671.do) which I find a must for anyone wants to get started with docker.

#Usage of Repo
```bash
cd path/to/folder
docker-composer build
docker-composer up -d
```

* * *
References:

Mouat,A. December 2015. Using Docker: Developing and Deploying Software with Containers 
O'Reilly Media, USA. 358 pp.
